<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>CLIENT WARUNG KITA | HOME</title>
</head>

<body>
    <div class="container-lg pt-5">

        <h2 class="text-center">DATA BARANG</h2>
        <a href="{{ route('create') }}" class="btn btn-success">Tambah Barang</a>

        <table class="table mt-4">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">NAMA</th>
                    <th scope="col">HARGA</th>
                    <th scope="col">JUMLAH</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">KETERANGAN</th>
                    <th scope="col">ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $dataBarang)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $dataBarang["name"] }}</td>
                    <td>{{ $dataBarang['price'] }}</td>
                    <td>{{ $dataBarang['quantity'] }}</td>
                    <td>{{ ($dataBarang['active'] == 1) ? 'Siap DiJual' : 'Belum Siap Jual' }}</td>
                    <td>{{ $dataBarang['description'] }}</td>
                    <td><button type="button" class="btn btn-primary">EDIT</button>
                        <button type="button" class="btn btn-danger">HAPUS</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

</body>

</html>