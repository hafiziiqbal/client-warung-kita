<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;



class DataController extends Controller
{
    public function index()
    {
        $response = Http::get('http://warung-kita.test/api/products');
        $data = $response->json();
        return view('index', compact('data'));
        // dd($data);
    }

    public function create()
    {
        return view('tambah');
    }
}
